<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'TableController@index');

Route::get('/table', 'TableController@table');

Route::get('/data-tables', 'TableController@dataTable');

// Cast Route
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('profile', 'ProfileController')->only(['index', 'update']);
    Route::resource('genre', 'GenreController');
    Route::resource('film', 'FilmController');
    Route::resource('peran', 'PeranController');
});

Route::resource('cast', 'CastController');

// Auth::routes();
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
