<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Latihan Laravel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex align-items-center">
        <div class="image">
          <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @guest
          <a href="#" class="d-block">Belum login</a>
          @endguest
          @auth
          <a href="/profile" class="d-block">{{ Auth::user()->name }}<br><small>{{ Auth::user()->profile->umur }} Tahun</small></a>
          @endauth
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link {{(request()->segment(1) == '') ? 'active' : ''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item {{(request()->segment(1) == 'table' || request()->segment(1) == 'data-tables') ? 'menu-open' : ''}}">
            <a href="#" class="nav-link {{(request()->segment(1) == 'table' || request()->segment(1) == 'data-tables') ? 'active' : ''}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="/table" class="nav-link {{(request()->segment(1) == 'table') ? 'active' : ''}}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Table</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/data-tables" class="nav-link {{(request()->segment(1) == 'data-tables') ? 'active' : ''}}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Data Tables</p>
                    </a>
                </li>
            </ul>
          </li>
          
          @auth
          <li class="nav-item">
            <a href="/cast" class="nav-link {{(request()->segment(1) == 'cast') ? 'active' : ''}}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Casts
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/genre" class="nav-link {{(request()->segment(1) == 'genre') ? 'active' : ''}}">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Genre
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/peran" class="nav-link {{(request()->segment(1) == 'peran') ? 'active' : ''}}">
              <i class="nav-icon fas fa-id-badge"></i>
              <p>
                Peran
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/film" class="nav-link {{(request()->segment(1) == 'film') ? 'active' : ''}}">
              <i class="nav-icon fas fa-video"></i>
              <p>
                Film
              </p>
            </a>
          </li>
          <li class="nav-item mt-3">
            <a href="{{ route('logout') }}" class="nav-link btn-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fas fa-sign-out-alt"></i>
              <p>&nbsp;Logout</p>
            </a>
            <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
          @endauth

          @guest
          <li class="nav-item mt-3">
            <a href="/login" class="nav-link btn-success">
              <i class="fas fa-sign-in-alt"></i>
              <p>&nbsp;Login</p>
            </a>
          </li>
          @endguest
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>