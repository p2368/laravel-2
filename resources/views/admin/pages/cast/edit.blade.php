@extends('admin.layout')

@section('title', 'Edit Cast ' . $cast->nama)

@section('content')
<a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
<form action="/cast/{{ $cast->id }}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" value="{{ $cast->nama }}">
      @error('nama')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" pattern="[1-9][0-9]*" class="form-control" name="umur" id="umur" value="{{ $cast->umur }}">
      @error('umur')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{ $cast->bio }}</textarea>
        @error('bio')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
    <input type="reset" value="Reset" class="btn btn-secondary">
  </form>
@endsection