@extends('admin.layout')

@section('title', 'Detail Cast ' . $cast->nama)

@section('content')
<dl class="row">
  <dt class="col-3">Nama</dt>
  <dd class="col-9">{{ $cast->nama }}</dd>

  <dt class="col-3">Umur</dt>
  <dd class="col-9">{{ $cast->umur }}</dd>

  <dt class="col-3">Bio</dt>
  <dd class="col-9">{{ $cast->bio }}</dd>
</dl>
<div class="back-button">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
</div>
@endsection