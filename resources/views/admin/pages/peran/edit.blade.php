@extends('admin.layout')

@section('title', 'Edit Peran ' . $peran->nama)

@section('content')
<a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
<form action="/peran/{{ $peran->id }}" method="POST">
  @method('put')
  @csrf
  <div class="form-group">
    <label for="nama">Nama</label>
    <input type="text" class="form-control" name="nama" id="nama" value="{{ $peran->nama }}">
    @error('nama')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="cast_id">Cast</label>
    <select name="cast_id" id="cast_id" class="custom-select">
      @forelse ($casts as $cast)
          <option value="{{ $cast->id }}" @if ($cast->id == $peran->cast_id)
              selected
          @endif
          >{{ $cast->nama }}</option>
      @empty
          <option disabled>BELUM ADA DATA</option>
      @endforelse
    </select>
    @error('cast_id')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="film_id">Judul Film</label>
    <select name="film_id" id="film_id" class="custom-select">
      @forelse ($films as $film)
          <option value="{{ $film->id }}" @if ($film->id == $peran->film_id)
              selected
          @endif>{{ $film->judul }}</option>
      @empty
          <option disabled>BELUM ADA DATA</option>
      @endforelse
    </select>
    @error('film_id')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
    @enderror
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  <input type="reset" value="Reset" class="btn btn-secondary">
</form>
@endsection