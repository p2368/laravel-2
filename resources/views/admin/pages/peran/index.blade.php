@extends('admin.layout')

@section('title', 'Peran')

@push('css')
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
@endpush

@section('content')
@auth
<a href="/peran/create" class="btn btn-primary mb-4">Tambah Data</a>
@endauth
@if (session()->has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Failed!</h4>
        <p>{{ session()->get('error') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif (session()->has('success')) 
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p>{{ session()->get('success') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<table id="peran" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th class="text-center">#</th>
              <th class="text-center">Peran</th>
              <th class="text-center">Nama Artis</th>
              <th class="text-center">Judul Film</th>
              <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($perans as $key => $peran)
                    <tr>
                        <td class="text-center">{{ $key+1 }}</td>
                        <td>{{ $peran->nama }}</td>
                        <td>{{ $peran->cast->nama }}</td>
                        <td>{{ $peran->film->judul }}</td>
                        <td class="text-center text-nowrap">
                            <a href="/peran/{{ $peran->id }}" class="btn btn-info btn-sm btn" data-toggle="tooltip" data-placement="top" title="Detail {{ $peran->nama }}" style="min-width: 34px;">
                                <i class="fa fa-info"></i>
                            </a>
                            @auth
                            <a href="/peran/{{ $peran->id }}/edit" class="btn btn-warning btn-sm text-white" data-toggle="tooltip" data-placement="top" title="Edit {{ $peran->nama }}" style="min-width: 34px;">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form action="/peran/{{ $peran->id }}" method="POST" class="d-inline-block">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus {{ $peran->nama }}" style="min-width: 34px;" onclick="return confirm('Anda yakin menghapus {{ $peran->nama }}  ?')">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            @endauth
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">DATA MASIH KOSONG</td>
                    </tr>
                @endforelse
                </tbody>
            <tfoot>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">Peran</th>
                <th class="text-center">Nama Artis</th>
                <th class="text-center">Judul Film</th>
                <th class="text-center">Action</th>
            </tr>
            </tfoot>
          </table>
@endsection

@push('js')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
      $("#peran").DataTable();
    });
</script>
@endpush