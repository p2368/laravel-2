@extends('admin.layout')

@section('title', 'Detail Peran ' . $peran->nama)

@section('content')
<dl class="row">
  <dt class="col-3">Diperankan oleh</dt>
  <dd class="col-9">{{ $peran->cast->nama }}</dd>
  <dt class="col-3">Film yang dibintangi</dt>
  <dd class="col-9">{{ $peran->film->judul }}
    
    {{-- <ul style="padding-left: 20px;">
      @forelse ($peran->film as $film)
          <li>{{ $film->judul }} ({{ $film->tahun }})</li>
      @empty
          <h4>DATA MASIH KOSONG</h4>
      @endforelse
    </ul> --}}
  </dd>
</dl>
<div class="back-button">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
</div>
@endsection