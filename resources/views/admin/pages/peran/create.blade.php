@extends('admin.layout')

@section('title', 'Create Peran')

@section('content')
<a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
<form action="/peran" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama">
      @error('nama')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="cast_id">Cast</label>
      <select name="cast_id" id="cast_id" class="custom-select">
        <option selected disabled>-- Pilih Cast --</option>
        @forelse ($casts as $cast)
            <option value="{{ $cast->id }}">{{ $cast->nama }}</option>
        @empty
            <option disabled>BELUM ADA DATA</option>
        @endforelse
      </select>
      @error('cast_id')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="film_id">Judul Film</label>
      <select name="film_id" id="film_id" class="custom-select">
        <option selected disabled>-- Pilih Film --</option>
        @forelse ($films as $film)
            <option value="{{ $film->id }}">{{ $film->judul }}</option>
        @empty
            <option disabled>BELUM ADA DATA</option>
        @endforelse
      </select>
      @error('film_id')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <input type="reset" value="Reset" class="btn btn-secondary">
  </form>
@endsection