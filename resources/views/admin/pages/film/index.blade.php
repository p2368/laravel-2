@extends('admin.layout')

@section('title', 'Film')

@section('content')
@if (session()->has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Failed!</h4>
        <p>{{ session()->get('error') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif (session()->has('success')) 
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p>{{ session()->get('success') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@auth
<a href="/film/create" class="btn btn-primary btn-sm mb-4">Tambah Film</a>
@endauth
<div class="row">
    @forelse ($films as $film)
    <div class="col-sm-4">
        <div class="card">
            <img src="{{asset('img/movies/'.$film->poster)}}" alt="{{ $film->judul }}" class="card-img-top img-responsive">
            <div class="card-body">
                <h4 class="card-title text-bold text-primary mb-1">{{ $film->judul }}</h4>
                <p class="card-text">{{ \Illuminate\Support\Str::limit($film->ringkasan, 100, '...') }} <a href="/film/{{ $film->id }}" class="text-bold text-dark">READ MORE</a></p>
                <div class="button-container text-right">
                    <a href="/film/{{ $film->id }}/edit" class="btn btn-warning btn-xs text-white" style="min-width: 24px;" data-toggle="tooltip" data-placement="top" title="Edit {{ $film->judul }}">
                        <i class="fa fa-edit"></i>
                    </a>
                    <form action="/film/{{ $film->id }}" method="POST" class="d-inline-block">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus {{ $film->judul }}" style="min-width: 24px;" onclick="return confirm('Anda yakin menghapus {{ $film->judul }}  ?')">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @empty
        <h4>DATA MASIH KOSONG</h4>
    @endforelse
</div>
@endsection
