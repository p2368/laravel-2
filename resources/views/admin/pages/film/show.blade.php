@extends('admin.layout')

@section('title', 'Detail Film ' . $film->judul)

@section('content')
<div class="row d-block">
  <ul class="list-unstyled d-none d-sm-block">
    <li class="media">
      <img src="{{ asset('img/movies/'.$film->poster) }}" alt="{{ $film->judul }}" class="mr-3 img-responsive" style="max-width: 250px">
      <div class="media-body">
        <h2 class="mt-0 mb-1 text-bold">{{ $film->judul }} ({{ $film->tahun }})</h2>
        <h6><span class="badge badge-warning">{{ $film->genre->nama }}</span></h6>
        <blockquote class="blockquote ml-0">
          <p class="mb-0">{{ $film->ringkasan }}</p>
          <footer class="blockquote-footer">Ringkasan oleh <cite title="Source Title">Latihan Laravel</cite></footer>
        </blockquote>
        <dl class="row">
          <dt class="col-2">Pemeran</dt>
          <dd class="col-10">
            @forelse ($film->peran as $peran)
                <a href="/cast/{{ $peran->cast->id }}" class="badge badge-secondary">{{ $peran->nama }} ({{ $peran->cast->nama }})</a>
            @empty
                Data Masih Kosong
            @endforelse
          </dd>
        </dl>
      </div>
    </li>
  </ul>
  <div class="d-block d-sm-none">
    <img src="{{ asset('img/movies/'.$film->poster) }}" alt="{{ $film->judul }}" class="mr-3 img-responsive w-100">
    <h2 class="mt-0 mb-1 text-bold">{{ $film->judul }} ({{ $film->tahun }})</h2>
        <h6><span class="badge badge-secondary">{{ $film->genre->nama }}</span></h6>
        <blockquote class="blockquote ml-0 mr-0 pr-0">
          <p class="mb-0">{{ $film->ringkasan }}</p>
          <footer class="blockquote-footer">Ringkasan oleh <cite title="Source Title">Latihan Laravel</cite></footer>
        </blockquote>
  </div>
    <div class="back-button">
      <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3 text-right"><i class="fa fa-reply"></i> Kembali</a>
    </div>
</div>
@endsection