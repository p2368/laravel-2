@extends('admin.layout')

@section('title', 'Create Film')

@section('content')
<a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="judul">Judul</label>
      <input type="text" class="form-control" name="judul" id="judul">
      @error('judul')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <label for="ringkasan">Ringkasan</label>
      <textarea name="ringkasan" id="ringkasan" class="form-control"></textarea>
      @error('ringkasan')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="text" name="tahun" id="tahun" class="form-control" pattern="[1-2][0-9][0-9][0-9]">
        @error('tahun')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label for="genre_id">Genre</label>
      <select name="genre_id" id="genre_id" class="custom-select">
        <option selected disabled>-- Pilih Genre --</option>
        @forelse ($genres as $genre)
            <option value="{{ $genre->id }}">{{ $genre->nama }}</option>
        @empty
            <option disabled>BELUM ADA DATA</option>
        @endforelse
      </select>
      @error('genre_id')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group">
      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <span class="input-group-text" id="uploadPoster">Upload</span>
        </div>
        <div class="custom-file">
          <input type="file" class="custom-file-input" name="poster" id="poster" aria-describedby="uploadPoster">
          <label class="custom-file-label" for="poster">Poster Film</label>
        </div>
      </div>
      @error('poster')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <input type="reset" value="Reset" class="btn btn-secondary">
  </form>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script>
      $(document).ready(function () {
        bsCustomFileInput.init()
      })
    </script>
@endpush