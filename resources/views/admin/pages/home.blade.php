@extends('admin.layout')

@section('title', 'Home')

@section('content')
    <h2 style="text-align: center">Latihan Blade Templating Laravel</h2>
    <p>
        <blockquote style="font-size: 1.5rem;"><em>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi porro soluta, eaque natus ab consectetur suscipit eligendi est molestias repudiandae ratione odio. Iusto quisquam accusamus tempore obcaecati soluta vel nobis!</em>
        </blockquote>
    </p>
    <p style="text-align: justify">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vero autem maiores temporibus? Suscipit enim adipisci aliquam harum quae laudantium, reprehenderit quibusdam dolor in voluptate cum, similique nobis debitis. Earum debitis repellendus officia vel necessitatibus libero blanditiis. Quos sed a odit, mollitia provident eveniet id fuga modi quasi similique, quod repellat asperiores quis reiciendis debitis deleniti nobis dolor tempore dignissimos unde necessitatibus? Nulla tempora a quibusdam nesciunt aliquam aliquid nam ullam, dicta commodi? Similique omnis debitis tempore officiis tenetur consequatur? Ipsa impedit ducimus dolore, porro exercitationem vitae rem nemo ex doloremque nobis placeat, vel facilis architecto reiciendis. Facere deserunt tenetur dignissimos nostrum, temporibus ut tempore quo dolorum incidunt modi quod! Magni, odit assumenda ipsum id earum iste voluptatem veritatis doloremque ducimus, ratione perferendis numquam rem repellendus natus excepturi vitae praesentium pariatur, consectetur obcaecati distinctio tempore ea sint quae. Animi, officiis quasi cumque labore amet dolore quisquam maxime est fuga odit esse doloribus a excepturi, at eligendi minus expedita voluptatibus ipsum ullam dicta repudiandae sapiente quo et maiores? Tempore laudantium non corporis expedita qui, et velit libero perspiciatis fuga, suscipit quam animi fugiat enim sed saepe natus dolor nostrum reiciendis laboriosam vero eum eius necessitatibus? Deleniti nam repellendus ullam, saepe tenetur eveniet?</p>
@endsection