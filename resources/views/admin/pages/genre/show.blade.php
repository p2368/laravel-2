@extends('admin.layout')

@section('title', 'Detail Genre ' . $genre->nama)

@section('content')
<dl class="row">
  <dt class="col-3">Nama</dt>
  <dd class="col-9">{{ $genre->nama }}</dd>
  <dt class="col-3">Daftar Film</dt>
  <dd class="col-9">
    <ul style="padding-left: 20px;">
      @forelse ($genre->film as $film)
          <li>{{ $film->judul }} ({{ $film->tahun }})</li>
      @empty
          <h4>DATA MASIH KOSONG</h4>
      @endforelse
    </ul>
  </dd>
</dl>
<div class="back-button">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
</div>
@endsection