@extends('admin.layout')

@section('title', 'Create Genre')

@section('content')
<a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama">
      @error('nama')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <input type="reset" value="Reset" class="btn btn-secondary">
  </form>
@endsection