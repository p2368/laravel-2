@extends('admin.layout')

@section('title', 'Edit Genre ' . $genre->nama)

@section('content')
<a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
<form action="/genre/{{ $genre->id }}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" value="{{ $genre->nama }}">
      @error('nama')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
    <input type="reset" value="Reset" class="btn btn-secondary">
  </form>
@endsection