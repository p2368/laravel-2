@extends('admin.layout')

@section('title', 'Profile ' . Auth::user()->name)

@push('css')
<link rel="stylesheet" href="{{asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
@endpush

@section('content')
    @if (session()->has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Failed!</h4>
        <p>{{ session()->get('error') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @elseif (session()->has('success')) 
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p>{{ session()->get('success') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <p class="login-box-msg">Anda dapat melakukan perubahan data diri Anda di halaman ini.</p>
    <form action="profile/{{ $profile->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group mb-3">
            <label for="name">Nama</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="Nama" value="{{ $profile->user->name }}" disabled>
        </div>
        @error('nama')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror

        <div class="form-group mb-3">
            <label for="email">Email</label>
            <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ $profile->user->email }}" disabled>
        </div>
        
        <div class="form-group mb-3">
            <label for="umur">Umur</label>
            <input type="text" name="umur" id="umur" class="form-control" placeholder="Umur" pattern="[1-9][0-9]*" value="{{ $profile->umur }}">
        </div>
        @error('umur')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror

        <div class="form-group mb-3">
            <label for="bio">Biodata</label>
            <textarea name="bio" id="bio" class="form-control" placeholder="Biodata">{{ $profile->bio }}</textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror

        <div class="form-group mb-3">
            <label for="alamat">Alamat Lengkap</label>
            <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat Lengkap Anda">{{ $profile->alamat }}</textarea>
        </div>
        @error('alamat')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror
        
        <div class="row">
            <!-- /.col -->
            <div class="col-12">
                <button type="submit" class="btn btn-primary d-block mx-auto mb-2">Simpan</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
@endsection