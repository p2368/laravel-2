<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = "kritik";
    protected $fillable = ['isi', 'point', 'user_id', 'film_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
}
