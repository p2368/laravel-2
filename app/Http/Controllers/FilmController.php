<?php

namespace App\Http\Controllers;

use App\Film;
use App\Genre;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::all();
        return view('admin.pages.film.index', compact('films'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::orderBy('nama')->get();
        return view('admin.pages.film.create', compact('genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'required|image|mimes:jpeg,jpg,png|max:1024'
        ]);

        $namaPoster = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('img/movies'), $namaPoster);

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $film->poster = $namaPoster;
        $film->save();

        if ($film) {
            return redirect('/film')->with('success', $film->judul . ' berhasil ditambahkan.');
        } else {
            return redirect('/film')->with('error', $film->judul . ' gagal ditambahkan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);
        return view('admin.pages.film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::findOrFail($id);
        $genres = Genre::orderBy('nama')->get();
        return view('admin.pages.film.edit', ['film' => $film, 'genres' => $genres]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'genre_id' => 'required',
            'poster' => 'image|mimes:jpeg,jpg,png|max:1024'
        ]);

        $film = Film::findOrFail($id);
        $namaTmp = $film->judul;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->genre_id = $request->genre_id;
        $posterTmp = $film->poster;

        if ($request->has('poster')) {
            $namaPoster = time() . '.' . $request->poster->extension();
            $request->poster->move(public_path('img/movies'), $namaPoster);
            $film->poster = $namaPoster;
            File::delete('img/movies/' . $posterTmp);
        }

        $result = $film->update();

        if ($result) {
            return redirect('/film')->with('success', $namaTmp . ' berhasil diperbaharui.');
        } else {
            return redirect('/film')->with('error', $namaTmp . ' gagal diperbaharui.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $path = "img/movies/";
        $film = Film::findOrfail($id);
        $namaTmp = $film->judul;
        $result = $film->delete();
        $resultFile = File::delete($path . $film->poster);

        if ($result == true && $resultFile == true) {
            return redirect('/film')->with('success', $namaTmp . ' berhasil dihapus.');
        } else {
            return redirect('/film')->with('error', $namaTmp . ' gagal dihapus.');
        }
    }
}
