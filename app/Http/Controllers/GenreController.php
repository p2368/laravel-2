<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class genreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genres = Genre::all();
        return view('admin.pages.genre.index', compact('genres'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.genre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $genre = new Genre;
        $genre->nama = $request->nama;
        $genre->save();

        if ($genre) {
            return redirect('/genre')->with('success', $genre->nama . ' berhasil ditambahkan.');
        } else {
            return redirect('/genre')->with('error', $genre->nama . ' gagal ditambahkan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::findOrFail($id);
        return view('admin.pages.genre.show', compact('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = Genre::findOrFail($id);
        return view('admin.pages.genre.edit', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $genre = Genre::findOrFail($id);
        $namaTmp = $genre->nama;
        $genre->nama = $request->nama;
        $genre->save();

        if ($genre) {
            return redirect('/genre')->with('success', $namaTmp . ' berhasil diubah menjadi ' . $genre->nama);
        } else {
            return redirect('/genre')->with('error', $namaTmp . ' gagal diubah.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::findOrFail($id);
        $namaTmp = $genre->nama;
        $result = $genre->delete();
        if ($result == true) {
            return redirect('/genre')->with('success', $namaTmp . ' berhasil dihapus.');
        } else {
            return redirect('/genre')->with('error', $namaTmp . ' gagal dihapus.');
        }
    }
}
