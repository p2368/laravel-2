<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function index()
    {
        return view('admin.pages.home');
    }

    public function table()
    {
        return view('admin.pages.table');
    }

    public function dataTable()
    {
        return view('admin.pages.data-tables');
    }
}
