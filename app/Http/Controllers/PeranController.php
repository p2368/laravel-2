<?php

namespace App\Http\Controllers;

use App\{
    Peran,
    Film,
    Cast
};
use Illuminate\Http\Request;

class PeranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perans = Peran::all();
        return view('admin.pages.peran.index', compact('perans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $films = Film::orderBy('judul')->get();
        $casts = Cast::orderBy('nama')->get();
        return view('admin.pages.peran.create', [
            'films' => $films,
            'casts' => $casts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'cast_id' => 'required',
            'film_id' => 'required',
        ]);

        $peran = new Peran;
        $peran->nama = $request->nama;
        $peran->cast_id = $request->cast_id;
        $peran->film_id = $request->film_id;
        $peran->save();

        if ($peran) {
            return redirect('/peran')->with('success', $peran->nama . ' berhasil ditambahkan.');
        } else {
            return redirect('/peran')->with('error', $peran->nama . ' gagal ditambahkan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $peran = Peran::findOrFail($id);
        return view('admin.pages.peran.show', compact('peran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peran = Peran::findOrFail($id);
        $films = Film::orderBy('judul')->get();
        $casts = Cast::orderBy('nama')->get();
        return view('admin.pages.peran.edit', [
            'peran' => $peran,
            'films' => $films,
            'casts' => $casts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'cast_id' => 'required',
            'film_id' => 'required',
        ]);

        $peran = Peran::findOrFail($id);
        $namaTmp = $peran->nama;
        $peran->nama = $request->nama;
        $peran->cast_id = $request->cast_id;
        $peran->film_id = $request->film_id;

        $result = $peran->update();

        if ($result) {
            return redirect('/peran')->with('success', $namaTmp . ' berhasil diperbaharui.');
        } else {
            return redirect('/peran')->with('error', $namaTmp . ' gagal diperbaharui.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peran = Peran::findOrfail($id);
        $namaTmp = $peran->nama;
        $result = $peran->delete();

        if ($result) {
            return redirect('/peran')->with('success', $namaTmp . ' berhasil dihapus.');
        } else {
            return redirect('/peran')->with('error', $namaTmp . ' gagal dihapus.');
        }
    }
}
