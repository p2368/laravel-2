<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('admin.pages.user.index', compact('profile'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required'
        ]);

        $profile = Profile::find($id);
        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamat = $request['alamat'];
        $result = $profile->save();

        if ($result == true) {
            return redirect('/profile')->with('success', 'Profile ' .  Auth::user()->name . ' berhasil diperbaharui.');
        } else {
            return redirect('/profile')->with('error', 'Profil ' . Auth::user()->name . 'gagal diperbaharui.');
        }
    }
}
