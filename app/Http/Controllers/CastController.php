<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Cast;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        // $casts = DB::table('cast')->get();
        // return view('admin.pages.cast.index', ['casts' => $casts]);
        $casts = Cast::all();
        return view('admin.pages.cast.index', compact('casts'));
    }

    public function create()
    {
        return view('admin.pages.cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $cast = DB::table('cast')->insert([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio']
        // ]);

        $cast = new Cast;
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $result = $cast->save();

        if ($result) {
            return redirect('/cast')->with('success', $cast->nama . ' berhasil ditambahkan.');
        } else {
            return redirect('/cast')->with('error', $cast->nama . ' gagal ditambahkan.');
        }
    }

    public function show($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();

        $cast = Cast::findOrFail($id);
        return view('admin.pages.cast.show', compact('cast'));
    }

    public function edit($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->get();
        $cast = Cast::findOrFail($id);
        return view('admin.pages.cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        // $cast = DB::table('cast')->where('id', $id)->update([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio']
        // ]);

        $cast = Cast::findOrFail($id);
        $namaTmp = $cast->nama;
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $result = $cast->update();

        if ($result) {
            return redirect('/cast')->with('success', $namaTmp . ' berhasil diperbaharui');
        } else {
            return redirect('/cast')->with('error', $namaTmp . ' gagal diperbaharui.');
        }
    }

    public function destroy($id)
    {
        // DB::table('cast')->where('id', $id)->delete();
        $cast = Cast::findOrFail($id);
        $namaTmp = $cast->nama;
        $result = $cast->delete();
        // $cast->delete();
        // Cast::destroy($id);
        if ($result == true) {
            return redirect('/cast')->with('success', $namaTmp . ' berhasil dihapus.');
        } else {
            return redirect('/cast')->with('error', $namaTmp . ' gagal dihapus.');
        }

        // return redirect('/cast');
    }
}
